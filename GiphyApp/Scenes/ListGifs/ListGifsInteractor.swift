//
//  ListGifsInteractor.swift
//  GiphyApp
//
//  Created by asylzhan on 11/4/17.
//  Copyright (c) 2017 asylzhan. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ListGifsBusinessLogic {
    func doSomething(request: ListGifs.FetchGifs.Request)
}

protocol ListGifsDataStore {
    //var name: String { get set }
}

class ListGifsInteractor: ListGifsBusinessLogic, ListGifsDataStore {
    var presenter: ListGifsPresentationLogic?
    var worker: ListGifsWorker?
    //var name: String = ""
    
    // MARK: Do something
    
    func doSomething(request: ListGifs.FetchGifs.Request) {
        worker = ListGifsWorker()
        worker?.doSomeWork()
        
        let response = ListGifs.FetchGifs.Response()
        presenter?.presentSomething(response: response)
    }
}
